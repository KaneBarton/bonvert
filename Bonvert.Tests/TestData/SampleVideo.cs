﻿using System.IO;
using System.Threading;

namespace Bonvert.Tests.TestData
{
    public static class SampleVideo
    {
        public const string ResourceName = "Bonvert.Tests.TestData.SampleVideo.m4v";
        public static readonly string SampleSource = Path.Combine(Path.GetTempPath(), "Bonvert-Source", ResourceName);

        public static void CreateLocalTestableFileFromResource()
        {
            var innerSampleSource = Path.GetDirectoryName(SampleSource) ?? Path.GetTempPath();

            if (!Directory.Exists(innerSampleSource))
            {
                Directory.CreateDirectory(SampleSource);
            }

            if (File.Exists(SampleSource))
            {
                return;
            }

            using (var memoryStream = new MemoryStream())
            {
                using (var resourceStream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(ResourceName))
                {
                    if (resourceStream != null)
                    {
                        resourceStream.CopyTo(memoryStream);
                        File.WriteAllBytes(SampleSource, memoryStream.ToArray());
                        Thread.Sleep(1);
                    }
                }
            }
        }
    }
}
