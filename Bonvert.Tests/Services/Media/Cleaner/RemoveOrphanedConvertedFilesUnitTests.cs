﻿using System.IO;
using System.Linq;
using Bonvert.Services.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bonvert.Tests.Services.Media.Cleaner
{
    [TestClass]
    public class RemoveOrphanedConvertedFilesUnitTests
    {
        private static string CreateUnitTestPath(string testName, string pathName)
        {
            return Path.Combine(Path.GetTempPath(), "ConvertMissingMediaFilesUnitTests", testName, pathName);
        }

        [TestMethod]
        public void RemovesFilesWhenOrphansExist()
        {
            // Arrange
            var libraryPath = CreateUnitTestPath("RemovesFilesWhenOrphansExist", "libraryPath");
            var outputPath = CreateUnitTestPath("RemovesFilesWhenOrphansExist", "outputPath");

            DirectoryAssistant.ReplaceDirectory(libraryPath);
            DirectoryAssistant.ReplaceDirectory(outputPath);

            var mediaLibrary = new Bonvert.Domain.MediaLibrary(outputPath, libraryPath); // the library and output paths are checkd to exist upon object construction
            var cleaner = new Bonvert.Services.Media.Cleaner(mediaLibrary, null);

            mediaLibrary.AddMedia("LF1");
            mediaLibrary.AddMedia("LF3");

            File.WriteAllBytes(Path.Combine(outputPath, "LF1"), new[] { new byte() });
            File.WriteAllBytes(Path.Combine(outputPath, "LF2"), new[] { new byte() });
            File.WriteAllBytes(Path.Combine(outputPath, "LF3"), new[] { new byte() });

            // Act
            cleaner.RemoveOrphanedConvertedFiles();

            // Assert
            Assert.AreEqual(mediaLibrary.Media.Count(), Directory.GetFiles(outputPath).Length);
        }

        [TestMethod]
        public void DoesNotRemoveAnyFilesWhenNoOrphansExist()
        {
            // Arrange
            var libraryPath = CreateUnitTestPath("DoesNotRemoveAnyFilesWhenNoOrphansExist", "libraryPath");
            var outputPath = CreateUnitTestPath("DoesNotRemoveAnyFilesWhenNoOrphansExist", "outputPath");

            DirectoryAssistant.ReplaceDirectory(libraryPath);
            DirectoryAssistant.ReplaceDirectory(outputPath);

            var mediaLibrary = new Bonvert.Domain.MediaLibrary(outputPath, libraryPath); // the library and output paths are checkd to exist upon object construction
            var cleaner = new Bonvert.Services.Media.Cleaner(mediaLibrary, null);

            mediaLibrary.AddMedia("LF1");
            mediaLibrary.AddMedia("LF2");
            mediaLibrary.AddMedia("LF3");

            File.WriteAllBytes(Path.Combine(outputPath, "LF1"), new[] { new byte() });
            File.WriteAllBytes(Path.Combine(outputPath, "LF2"), new[] { new byte() });
            File.WriteAllBytes(Path.Combine(outputPath, "LF3"), new[] { new byte() });

            // Act
            cleaner.RemoveOrphanedConvertedFiles();

            // Assert
            Assert.AreEqual(3, Directory.GetFiles(outputPath).Length);
        }
    }
}
