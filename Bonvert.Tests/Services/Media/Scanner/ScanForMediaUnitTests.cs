﻿using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Bonvert.Tests.Services.Media.Scanner
{
    [TestClass]
    public class ScanForMediaUnitTests
    {
        [TestMethod]
        public void InvalidExtensionDoesNotAddAnyItemsToCollection()
        {
            // Arrange
            var mediaLibrary = new Bonvert.Domain.MediaLibrary(Path.GetTempPath(), Path.GetTempPath());

            var mock = new Mock<Bonvert.Services.Media.Scanner>(mediaLibrary, null);
            mock.Setup(x => x.IsValidExtension("File1.mkv")).Returns(false);
            mock.Setup(x => x.IsValidExtension("File2.avi")).Returns(false);
            mock.Setup(x => x.IsValidExtension("File3.MP4")).Returns(false);
            mock.Setup(x => x.IsValidExtension("File5.m2ts")).Returns(false);
            mock.Setup(x => x.ScanFiles(Path.GetTempPath())).Returns(new[] {"File1.mkv", "File2.avi", "File3.MP4", "File5.m2ts"});

            // Act
            mock.Object.ScanForMedia();

            // Assert
            Assert.AreEqual(0, mediaLibrary.Media.Count());
        }

        [TestMethod]
        public void ValidExtensionAddsItemsToCollection()
        {
            // Arrange
            var mediaLibrary = new Bonvert.Domain.MediaLibrary(Path.GetTempPath(), Path.GetTempPath());

            var mock = new Mock<Bonvert.Services.Media.Scanner>(mediaLibrary, null);
            mock.Setup(x => x.ScanForMedia()).CallBase();

            // Act
            mock.Object.ScanForMedia();

            // Assert
            Assert.AreNotEqual(0, mediaLibrary.Media.Count());
        }
    }
}
