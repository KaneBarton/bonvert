﻿using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bonvert.Tests.Services.Media.Scanner
{
    [TestClass]
    public class ScanFilesUnitTests
    {
        [TestMethod]
        public void ReturnsFiles()
        {
            // Arrange
            var scanner = new Bonvert.Services.Media.Scanner(null, null);
            Path.GetTempFileName();

            // Act
            var files = scanner.ScanFiles(Path.GetTempPath());

            // Asserts
            Assert.AreNotEqual(0, files.Count());
        }
    }
}
