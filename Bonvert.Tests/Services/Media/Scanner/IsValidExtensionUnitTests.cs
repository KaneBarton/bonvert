﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bonvert.Tests.Services.Media.Scanner
{
    [TestClass]
    public class IsValidExtensionUnitTests
    {
        [TestMethod]
        public void WhenNotMediaExtensionThenFalse()
        {
            // Arrange
            var scanner = new Bonvert.Services.Media.Scanner(null, null);

            // Act
            var txt = scanner.IsValidExtension("somefile.txt");

            // Assert
            Assert.IsFalse(txt);
        }

        [TestMethod]
        public void WhenMediaExtensionThenTrue()
        {
            // Arrange
            var scanner = new Bonvert.Services.Media.Scanner(null, null);

            // Act
            var mkv = scanner.IsValidExtension("somefile.mkv");
            var avi = scanner.IsValidExtension("SOMEFILE.AVI");
            var mp4 = scanner.IsValidExtension("somefile.mp4");
            var m2 = scanner.IsValidExtension("somefile.m2Ts");

            // Assert
            Assert.IsTrue(mkv);
            Assert.IsTrue(avi);
            Assert.IsTrue(mp4);
            Assert.IsTrue(m2);
        }

    }
}
