﻿using System.IO;
using Bonvert.Domain;
using Bonvert.Services.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bonvert.Tests.Services.Media.Converter
{
    [TestClass]
    public class ConvertMissingMediaFilesUnitTests
    {
        private static string CreateUnitTestPath(string testName, string pathName)
        {
            return Path.Combine(Path.GetTempPath(), "ConvertMissingMediaFilesUnitTests", testName, pathName);
        }

        [TestMethod]
        public void ReturnsZeroWhenNoFilesToConvert()
        {
            // Arrange
            var libraryPath = CreateUnitTestPath("ReturnsZeroWhenNoFilesToConvert", "libraryPath");
            var outputPath = CreateUnitTestPath("ReturnsZeroWhenNoFilesToConvert", "outputPath");

            DirectoryAssistant.ReplaceDirectory(libraryPath);
            DirectoryAssistant.ReplaceDirectory(outputPath);

            var mediaLibrary = new MediaLibrary(outputPath, libraryPath);
            var converter = new Bonvert.Services.Media.Converter(mediaLibrary, null, null);

            // create a file that has already been converted
            mediaLibrary.AddMedia(Path.Combine(libraryPath, "FileThatHasBeenConverted.mkv"));
            File.WriteAllBytes(Path.Combine(outputPath, "FileThatHasBeenConverted.m4v"), new[] { new byte(), new byte() });

            // Act
            converter.ConvertMissingMediaFiles();
            var actual = converter.ConvertedFiles;

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void ReturnsNumberOfConvertedFilesWhenFilesAreRequiredForConversion()
        {
            // Arrange
            var libraryPath = CreateUnitTestPath("ReturnsNumberOfConvertedFilesWhenFilesAreRequiredForConversion", "libraryPath");
            var outputPath = CreateUnitTestPath("ReturnsNumberOfConvertedFilesWhenFilesAreRequiredForConversion", "outputPath");

            DirectoryAssistant.ReplaceDirectory(libraryPath);
            DirectoryAssistant.ReplaceDirectory(outputPath);

            var mediaLibrary = new MediaLibrary(outputPath, libraryPath);
            var converter = new Bonvert.Services.Media.Converter(mediaLibrary, null, null);

            // create a file that has already been converted
            mediaLibrary.AddMedia(Path.Combine(libraryPath, "FileThatHasBeenConverted.mkv"));
            File.WriteAllBytes(Path.Combine(outputPath, "FileThatHasBeenConverted.m4v"), new[] { new byte(), new byte() });

            // create a file that has NOT been converted
            mediaLibrary.AddMedia("NewNotConverted.mkv");
            File.WriteAllBytes(Path.Combine(libraryPath, "NewNotConverted.mkv"), new[] { new byte(), new byte() });

            // Act
            converter.ConvertMissingMediaFiles();
            var actual = converter.ConvertedFiles;

            // Assert
            Assert.AreEqual(1, actual);
        }
    }
}
