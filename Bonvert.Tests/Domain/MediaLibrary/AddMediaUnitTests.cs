﻿using System.Linq;
using Bonvert.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bonvert.Tests.Domain.MediaLibrary
{
    [TestClass]
    public class AddMediaUnitTests
    {
        [TestMethod]
        public void MediaFilesAddedToCollectionWhenAddMethodIsExecuted()
        {
            // Arrange
            IMediaLibrary mediaLibrary = new Bonvert.Domain.MediaLibrary(null, null);

            // Act
            mediaLibrary.AddMedia("SomeFile.m4v");

            // Assert
            Assert.AreNotEqual(0, mediaLibrary.Media.Count());
        }
    }
}
