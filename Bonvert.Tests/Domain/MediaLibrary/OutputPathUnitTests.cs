﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bonvert.Tests.Domain.MediaLibrary
{
    [TestClass]
    public class OutputPathUnitTests
    {
        [TestMethod]
        [ExpectedException(typeof(DirectoryNotFoundException))]
        public void ThrowsExceptionWhenDirectoryDoesNotExist()
        {
            var mediaLibrary = new Bonvert.Domain.MediaLibrary(@"C:\Users\Kane\Desktop", @"C:\Users\Kane\Desktop", "SOME INVALID PATH", @"C:\Users\Kane\Downloads");
        }
    }
}
