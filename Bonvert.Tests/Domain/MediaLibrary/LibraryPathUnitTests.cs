﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bonvert.Tests.Domain.MediaLibrary
{
    [TestClass]
    public class LibraryPathUnitTests
    {
        [TestMethod]
        [ExpectedException(typeof(DirectoryNotFoundException))]
        public void ThrowsExceptionWhenAnyDirectoryDoesNotExist()
        {
            // Arrange and Act
            var mediaLibrary = new Bonvert.Domain.MediaLibrary("SomeINalidPath", @"C:\Users\Kane\Desktop", "SOME INVALID PATH", @"C:\Users\Kane\Downloads");
        }

        [TestMethod]
        public void ReturnsStringWhenSet()
        {
            // Arrange
            var mediaLibrary = new Bonvert.Domain.MediaLibrary(Path.GetTempPath(), Path.GetTempPath(), Path.GetTempPath());

            // Act
            var result = mediaLibrary.LibraryPath;

            // Assert
            Assert.IsNotNull(result);
        }

    }
}
