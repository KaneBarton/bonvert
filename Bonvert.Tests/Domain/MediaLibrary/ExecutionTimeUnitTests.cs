﻿using System.Threading;
using Bonvert.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bonvert.Tests.Domain.MediaLibrary
{
    [TestClass]
    public class ExecutionTimeUnitTests
    {
        [TestMethod]
        public void HasValidTimeSpanWhenInstantiated()
        {
            // Arrange
            IMediaLibrary mediaLibrary = new Bonvert.Domain.MediaLibrary(null, null);
            Thread.Sleep(1);

            // Act
            var actual = mediaLibrary.ExecutionTime();

            // Assert
            Assert.AreNotEqual(0, actual.Milliseconds);
        }
    }
}
