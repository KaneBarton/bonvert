﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bonvert.Tests.Domain.MediaModel
{
    [TestClass]
    public class IsLargerThanFat32UnitTests
    {
        [TestMethod]
        public void ReturnsFalseWhenSmallFile()
        {
            // Arrange
            var fileName = Path.GetTempFileName();
            var model = new Bonvert.Domain.MediaModel(fileName, Path.GetTempPath());

            // Simulate a converted M4V file
            File.WriteAllBytes(Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(fileName) + ".m4v"), new[] { new byte() });

            // Act
            var actual = model.IsLargerThanFat32;

            // Assert
            Assert.IsFalse(actual);
        }
    }
}
