﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bonvert.Tests.Domain.MediaModel
{
    [TestClass]
    public class ConvertedFileUnitTests
    {
        [TestMethod]
        public void ReturnsEmptyStringWhenNotConverted()
        {
            // Arrange
            var model = new Bonvert.Domain.MediaModel(Path.GetTempFileName(), Path.GetTempPath());

            // Act
            var result = model.ConvertedFile;

            // Assert
            Assert.IsTrue(string.IsNullOrEmpty(result));
        }
    }
}
