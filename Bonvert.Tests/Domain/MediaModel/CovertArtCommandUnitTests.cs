﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bonvert.Tests.Domain.MediaModel
{
    [TestClass]
    public class CovertArtCommandUnitTests
    {
        [TestMethod]
        public void ReturnString()
        {
            // Arrange
            var model = new Bonvert.Domain.MediaModel("FileName", "ConversionPath");
            
            // Act
            var result = model.CovertArtCommand;

            // Assert
            Assert.IsFalse(string.IsNullOrEmpty(result));
        }
    }
}
