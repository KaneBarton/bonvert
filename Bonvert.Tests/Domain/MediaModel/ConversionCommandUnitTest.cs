﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bonvert.Tests.Domain.MediaModel
{
    [TestClass]
    public class ConversionCommandUnitTest
    {
        [TestMethod]
        public void ReturnString()
        {
            // Arrange
            var model = new Bonvert.Domain.MediaModel("FileName", "ConversionPath");

            // Act
            var result = model.ConversionCommand;

            // Assert
            Assert.IsFalse(string.IsNullOrEmpty(result));
        }
    }
}
