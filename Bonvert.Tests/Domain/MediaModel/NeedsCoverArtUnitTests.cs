﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using File = System.IO.File;

namespace Bonvert.Tests.Domain.MediaModel
{
    [TestClass]
    public class NeedsCoverArtUnitTests
    {
        [TestMethod]
        public void TrueWhenNoCoverArtExists()
        {
            // Arrange
            var fileName = Path.GetTempFileName();
            var model = new Bonvert.Domain.MediaModel(fileName, Path.GetTempPath());

            // Simulate a converted M4V file
            File.WriteAllBytes(Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(fileName) + ".m4v"), new[] { new byte() });

            // Act
            var actual = model.IsLargerThanFat32;

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void FalseWhenCoverArtExists()
        {
            // Arrange
            var model = new Bonvert.Domain.MediaModel(TestData.SampleVideo.ResourceName, Path.GetDirectoryName(TestData.SampleVideo.SampleSource));

            // Create the sample file
            TestData.SampleVideo.CreateLocalTestableFileFromResource();

            // Act
            var actual = model.NeedsCoverArt;

            // Assert
            Assert.IsFalse(actual);
        }
    }
}
