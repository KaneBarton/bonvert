﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bonvert.Tests.Domain.MediaModel
{
    [TestClass]
    public class HasDownloadedCoverArtUnitTests
    {
        [TestMethod]
        public void IsFalseWhenFileDoesNotExist()
        {
            // Arrange
            var model = new Bonvert.Domain.MediaModel(Path.GetTempFileName(), Path.GetTempPath());

            // Act
            var downloadedCoverArt = model.HasDownloadedCoverArt;

            // Assert
            Assert.IsFalse(downloadedCoverArt);
        }

        [TestMethod]
        public void IsTrueWhenFileExists()
        {
            // Arrange
            var model = new Bonvert.Domain.MediaModel(Path.GetTempFileName(), Path.GetTempPath());
            File.WriteAllBytes(model.CoverArtFileName, new[] { new byte() });

            // Act
            var downloadedCoverArt = model.HasDownloadedCoverArt;

            // Assert
            Assert.IsTrue(downloadedCoverArt);
        }
    }
}
