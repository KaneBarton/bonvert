﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bonvert.Tests.Domain.MediaModel
{
    [TestClass]
    public class ConvertedFileSizeUnitTests
    {
        // zero for non converted
        // > 1 for converted

        [TestMethod]
        public void IsZeroWhenNotConverted()
        {
            // Arrange
            var model = new Bonvert.Domain.MediaModel(Path.GetTempFileName(), Path.GetTempPath());

            // Act
            var fileSize = model.ConvertedFileSize;

            // Assert
            Assert.AreEqual(0, fileSize);
        }

        [TestMethod]
        public void IsGreaterThanZeroWhenConvertedToM4V()
        {
            // Arrange
            var fileName = Path.GetTempFileName();
            var model = new Bonvert.Domain.MediaModel(fileName, Path.GetTempPath());

            // Simulate a converted M4V file
            File.WriteAllBytes(Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(fileName) + ".m4v"), new[] { new byte() });

            // Act
            var fileSize = model.ConvertedFileSize;

            // Assert
            Assert.AreNotEqual(0, fileSize);
        }

        [TestMethod]
        public void IsGreaterThanZeroWhenConvertedToMP4()
        {
            // Arrange
            var fileName = Path.GetTempFileName();
            var model = new Bonvert.Domain.MediaModel(fileName, Path.GetTempPath());

            // Simulate a converted MP4 file
            File.WriteAllBytes(Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(fileName) + ".mp4"), new[] { new byte() });

            // Act
            var fileSize = model.ConvertedFileSize;

            // Assert
            Assert.AreNotEqual(0, fileSize);
        }


    }
}
