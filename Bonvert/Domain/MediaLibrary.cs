﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using Bonvert.Annotations;

namespace Bonvert.Domain
{
    [UsedImplicitly]
    public class MediaLibrary : IMediaLibrary
    {
        private string _outputPath;
        private string[] _libraryPath;
        private readonly Stopwatch _timerStopwatch = new Stopwatch();
        private readonly IList<MediaModel> _media = new List<MediaModel>(); 

        public MediaLibrary()
            : this(ConfigurationManager.AppSettings["OutputPath"], ConfigurationManager.AppSettings["LibrayPath"].Split(';'))
        {}

        public MediaLibrary(string outputPath, params string[] libraryPath)
        {
            OutputPath = outputPath;
            LibraryPath = libraryPath;

            NonMappedPaths = new List<string>();
            AllPathsMapped = true;

            _timerStopwatch.Start();   
        }

        public IEnumerable<MediaModel> Media {
            get
            {
                return _media;
            }
        }

        public string OutputPath
        {
            get
            {
                return _outputPath;
            }
            private set
            {
                var innerValue = value;
                if (string.IsNullOrEmpty(innerValue))
                {
                    return;
                }

                if (!Directory.Exists(innerValue))
                {
                    throw new DirectoryNotFoundException(innerValue);
                }

                _outputPath = innerValue;
            }
        }

        public bool AllPathsMapped { get; private set; }

        public IList<string> NonMappedPaths { get; private set; } 

        public string[] LibraryPath
        {
            get
            {
                return _libraryPath;
            }
            private set
            {
                var innerValue = value;
                if (innerValue == null)
                {
                    return;
                }

                foreach (var path in innerValue)
                {
                    if (!Directory.Exists(path))
                    {
                        NonMappedPaths.Add(path);
                        AllPathsMapped = false;
                    }                    
                }

                _libraryPath = innerValue;
                
            }
        }

        public void AddMedia(string fileName)
        {
            _media.Add(new MediaModel(fileName, OutputPath));
        }

        public TimeSpan ExecutionTime()
        {
            return _timerStopwatch.Elapsed;
        }
    }
}