﻿using System;
using System.IO;
using System.Linq;
using TagLib;
using File = System.IO.File;

namespace Bonvert.Domain
{
    public class MediaModel
    {
        private bool? _needsCoverArt;

        public MediaModel(string fileName, string conversionPath)
        {
            FileName = fileName;
            ConversionPath = conversionPath;
        }

        public string FileName { get; private set; }
        public string ConversionPath { get; private set; }

        public string Name
        {
            get
            {
                return Path.GetFileNameWithoutExtension(FileName);
            }
        }

        public string ConvertedFile
        {
            get
            {
                if (IsM4V) return Path.Combine(ConversionPath, string.Format("{0}.m4v", Name));
                if (IsMP4) return Path.Combine(ConversionPath, string.Format("{0}.mp4", Name));

                return string.Empty;
            }
        }

        public long ConvertedFileSize
        {
            get
            {
                if (IsConverted)
                {
                    return new FileInfo(ConvertedFile).Length;
                }

                return 0;
            }
        }

        public string CoverArtFileName
        {
            get
            {
                return Path.Combine(Path.GetTempPath(), string.Format("{0}.jpg", Name));
            }
        }

        public bool IsConverted
        {
            get
            {
                return IsM4V || IsMP4;
            }
        }

        public bool IsM4V
        {
            get
            {
                return File.Exists(Path.Combine(ConversionPath, string.Format("{0}.m4v", Name)));
            }
        }

        public bool IsMP4
        {
            get
            {
                return File.Exists(Path.Combine(ConversionPath, string.Format("{0}.mp4", Name)));
            }
        }

        public bool IsLargerThanFat32
        {
            get
            {
                return (ConvertedFileSize >> 30) >= 2;
            }
        }

        public bool HasDownloadedCoverArt
        {
            get
            {
                return File.Exists(CoverArtFileName);
            }
        }

        public bool NeedsCoverArt
        {
            get
            {
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    return true;
                }

                if (_needsCoverArt.HasValue)
                {
                    return _needsCoverArt.Value;
                }

                try
                {
                    Console.Write(".");
                    var tag = TagLib.File.Create(ConvertedFile).Tag;
                    _needsCoverArt =  !tag.Pictures.Any();
                }
                catch (CorruptFileException)
                {
                    _needsCoverArt = true;
                }

                return _needsCoverArt.Value;
            }
        }

        public string ConversionCommand
        {
            get
            {
                return string.Format(@" -i  ""{0}"" -o ""{1}\{2}.m4v"" --preset=""iPad"" ", FileName, ConversionPath, Name);
            }
        }

        public string CovertArtCommand
        {
            get
            {
                return string.Format(@" ""{0}"" -W --artwork ""{1}"" ", ConvertedFile, CoverArtFileName.RemoveTrailingSlash());
            }
        }

        public void RenameToMp4()
        {
            File.Move(ConvertedFile, ConvertedFile.Replace(".m4v", ".mp4"));
        }
    }
}