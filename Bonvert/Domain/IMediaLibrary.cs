﻿using System;
using System.Collections.Generic;

namespace Bonvert.Domain
{
    public interface IMediaLibrary
    {
        IEnumerable<MediaModel> Media { get; }

        void AddMedia(string fileName);
        
        string[] LibraryPath { get; }
        string OutputPath { get; }
        bool AllPathsMapped { get; }

        IList<string> NonMappedPaths { get; }

        TimeSpan ExecutionTime();
    }
}