﻿using System;
using System.Linq;
using Autofac;
using Bonvert.Domain;
using Bonvert.Services.Media.Interfaces;

namespace Bonvert
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                if (args.Any())
                {
                    ProcessCoverArtOnly();
                }
                else
                {
                    Process();
                }
            }
            catch (Exception exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(exception);
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

        private static void ProcessCoverArtOnly()
        {
            Console.WriteLine("Processing Cover Art Only");
            Console.WriteLine(DateTime.Now);
            Console.WriteLine();

            var mediaLibrary = Application.Container.Resolve<IMediaLibrary>();

            var mediaScanner = Application.Container.Resolve<IRegistry>();


            // Apply app.config settings
            // Clean library
            // Apply cover art
            // Rename > 2GB M4V
            // Start conversion
            mediaScanner.RegisterApplicationSettings()
                        .ScanForMedia()
                        .RemoveOrphanedConvertedFiles()
                        .LogMediaFilesToConvert()
                        .LogMediaLargerThanFat32()
                        .ApplyCoverArtForConvertedMedia();

            Console.WriteLine();
            Console.WriteLine("DONE");
            Console.WriteLine(mediaLibrary.ExecutionTime());

            if (System.Diagnostics.Debugger.IsAttached)
            {
                Console.Read();
            }
        }

        private static void Process()
        {
            Console.WriteLine(DateTime.Now);
            Console.WriteLine();

            var mediaLibrary = Application.Container.Resolve<IMediaLibrary>();

            var mediaScanner = Application.Container.Resolve<IRegistry>();

            // Apply app.config settings
            // Clean library
            // Apply cover art
            // Rename > 2GB M4V
            // Start conversion
            mediaScanner.RegisterApplicationSettings()
                        .ScanForMedia()
                        .RemoveOrphanedConvertedFiles()
                        .ConvertMissingMediaFiles()
                        .RenameMediaLargerThanFat32()
                        .ApplyCoverArtForConvertedMedia();

            Console.WriteLine();
            Console.WriteLine("DONE");
            Console.WriteLine(mediaLibrary.ExecutionTime());

            if (System.Diagnostics.Debugger.IsAttached)
            {
                Console.Read();
            }
        }
    }
}
