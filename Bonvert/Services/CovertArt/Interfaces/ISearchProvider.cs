﻿using Bonvert.Domain;

namespace Bonvert.Services.CovertArt.Interfaces
{
    public interface ISearchProvider
    {
        void SearchForAndDownloadCovertArt(MediaModel model);
    }
}