﻿namespace Bonvert.Services.CovertArt.Interfaces
{
    public interface IIMDBLookup
    {
        string RetrieveCoverArtUriByImdbIdentifier(string imdbIdentifier);
    }
}