﻿using Bonvert.Annotations;
using Bonvert.Services.CovertArt.Interfaces;
using Newtonsoft.Json;

namespace Bonvert.Services.CovertArt
{
    [UsedImplicitly]
    public class IMDBLookup : IIMDBLookup
    {
        public string RetrieveCoverArtUriByImdbIdentifier(string imdbIdentifier)
        {
            var jsonResponse = HttpGet(string.Format("http://www.omdbapi.com/?i={0}", imdbIdentifier));
            var movieData = JsonConvert.DeserializeObject<JMovie>(jsonResponse);

            return movieData.Poster;
        }

        private string HttpGet(string uri)
        {
            if (string.IsNullOrEmpty(uri)) return string.Empty;

            var req = System.Net.WebRequest.Create(uri);
            req.Timeout = 5 * 1000;
            req.Proxy = null;
            using (var resp = req.GetResponse())
            {
                var sr = new System.IO.StreamReader(resp.GetResponseStream());
                return sr.ReadToEnd().Trim();
            }
        }

        [UsedImplicitly]
        private class JMovie
        {
            public string Poster { get; set; }
            
            public bool IsValid()
            {
                return !(string.IsNullOrEmpty(Poster));
            }
        }
    }
}