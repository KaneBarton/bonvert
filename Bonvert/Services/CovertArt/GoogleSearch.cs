﻿using System;
using System.Linq;
using Bonvert.Domain;
using Bonvert.Services.CovertArt.Interfaces;
using Bonvert.Services.Infrastructure.Interfaces;
using Google.Apis.Customsearch.v1;
using Google.Apis.Customsearch.v1.Data;
using Google.Apis.Services;

namespace Bonvert.Services.CovertArt
{
    public class GoogleSearch : ISearchProvider
    {
        private static readonly BaseClientService.Initializer Initializer = new BaseClientService.Initializer
        {
            ApiKey = "AIzaSyAoKx74zlDd_kjxLoN9XNiHWckNGObSmFE",
            ApplicationName = "Bonvert",
        };
        
        private readonly IDownloadAssistant _downloadAssistant;
        private readonly IIMDBLookup _imdbLookup;

        public GoogleSearch(IDownloadAssistant downloadAssistant, IIMDBLookup imdbLookup)
        {
            _downloadAssistant = downloadAssistant;
            _imdbLookup = imdbLookup;
        }


        public void SearchForAndDownloadCovertArt(MediaModel model)
        {
            var searchResult = SearchImdb(model.Name);
            if (searchResult == null)
            {
                return;
            }

            string imdbId;
            if (!TryParseImdbId(searchResult.Link, out imdbId))
            {
                return;
            }

            var imdbImage = _imdbLookup.RetrieveCoverArtUriByImdbIdentifier(imdbId);
                    
            _downloadAssistant.DownloadFile(imdbImage, model.CoverArtFileName);
        }

        private Result SearchImdb(string movieName)
        {
            try
            {
                var service = new CustomsearchService(Initializer);
                var request = service.Cse.List(movieName);
                request.Cx = "002681121051117809948:jsfnmqvglua";

                return request.Execute().Items.FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        private bool TryParseImdbId(string link, out string imdbId)
        {
            imdbId = string.Empty;

            if (!link.Contains("/tt")) return false;

            imdbId = link.Substring(link.IndexOf("/tt", StringComparison.InvariantCultureIgnoreCase) + 1);
            if (imdbId.EndsWith("/"))
            {
                imdbId = imdbId.Substring(0, imdbId.Length - 1);
            }

            return true;
        }
    }
}