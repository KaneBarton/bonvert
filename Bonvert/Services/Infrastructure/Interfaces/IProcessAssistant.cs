﻿using System.Diagnostics;

namespace Bonvert.Services.Infrastructure.Interfaces
{
    public interface IProcessAssistant
    {
        void DisplayCommandData(object sender, DataReceivedEventArgs e);
    }
}