﻿namespace Bonvert.Services.Infrastructure.Interfaces
{
    public interface IFileAssistant
    {
        bool IsFileLocked(string filePath, int maximumTimeoutSeconds);
    }
}