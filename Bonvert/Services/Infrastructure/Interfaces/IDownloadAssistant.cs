﻿namespace Bonvert.Services.Infrastructure.Interfaces
{
    public interface IDownloadAssistant
    {
        void DownloadFile(string uri, string destinationFilePath);
    }
}