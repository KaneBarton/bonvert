﻿namespace Bonvert.Services.Infrastructure.Interfaces
{
    public interface IImageAspectAssistance
    {
        void ResizeImageToCorrectAspectRatio(string imagePath);
    }
}