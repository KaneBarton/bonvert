﻿using System.Diagnostics;
using System.IO;
using System.Threading;
using Bonvert.Services.Infrastructure.Interfaces;

namespace Bonvert.Services.Infrastructure
{
    public class FileAssistant : IFileAssistant
    {
        public bool IsFileLocked(string filePath, int maximumTimeoutSeconds)
        {
            var timeout = maximumTimeoutSeconds * 1000;

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            while (IsFileLocked(filePath))
            {
                Thread.Sleep(500);
                if (stopwatch.ElapsedMilliseconds > timeout)
                {
                    return true;
                }
            }
            stopwatch.Stop();

            return false;
        }

        private bool IsFileLocked(string filePath)
        {
            try
            {
                if (!File.Exists(filePath))
                {
                    return false;
                }

                using (File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    return false;
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
        }
    }
}
