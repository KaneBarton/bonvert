﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Bonvert.Services.Infrastructure
{
    public static class ImageAssistant
    {
        public static Bitmap CreateThumbnail(string fileName, out string dimensions)
        {
            return CreateThumbnail(fileName, 96, 96, out dimensions);
        }

        public static Bitmap CreateThumbnail(string fileName, int width, int height, out string dimensions)
        {
            dimensions = "1920x1080";

            Bitmap result;
            try
            {
                using (var ms = new MemoryStream(File.ReadAllBytes(fileName)))
                {
                    using (var bitmapFile = new Bitmap(ms))
                    {
                        dimensions = $"{bitmapFile.Width}x{bitmapFile.Height}";


                        var ratio = 0M;
                        int newWidth;
                        int newHeight;

                        //*** If the image is smaller than a thumbnail just return it
                        if (bitmapFile.Width < width && bitmapFile.Height < height)
                        {
                            return bitmapFile;
                        }

                        if (bitmapFile.Width > bitmapFile.Height)
                        {
                            dimensions = dimensions + " Landscape";

                            ratio = (decimal)width / bitmapFile.Width;
                            newWidth = width;
                            var lnTemp = bitmapFile.Height * ratio;
                            newHeight = (int)lnTemp;
                        }
                        else
                        {
                            dimensions = dimensions + " Portrait";

                            ratio = (decimal)height / bitmapFile.Height;
                            newHeight = height;
                            var lnTemp = bitmapFile.Width * ratio;
                            newWidth = (int)lnTemp;
                        }

                        result = new Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb);
                        var g = Graphics.FromImage(result);
                        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        g.FillRectangle(Brushes.Black, 0, 0, newWidth, newHeight);
                        g.DrawImage(bitmapFile, 0, 0, newWidth, newHeight);
                    }
                }
            }
            catch
            {
                using (var ms = new MemoryStream(File.ReadAllBytes("ErrorImage.png")))
                {
                    return new Bitmap(ms);
                }
            }

            return result;
        }

        public static Image ResizeImg(Image img, int width)
        {
            // 16:9 Aspect Ratio. You can also add it as parameters
            var aspectRatio_X = 16M;
            var aspectRatio_Y = 9M;
            var targetHeight = Convert.ToDecimal(width) / (aspectRatio_X / aspectRatio_Y);

            img = CropImg(img);
            var bmp = new Bitmap(width, (int)targetHeight, PixelFormat.Format32bppArgb);
            var grp = Graphics.FromImage(bmp);
            grp.DrawImage(img, new Rectangle(0, 0, bmp.Width, bmp.Height), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);
            return bmp;

        }

        public static Image CropImg(Image img)
        {
            // 16:9 Aspect Ratio. You can also add it as parameters
            var aspectRatio_X = 16M;
            var aspectRatio_Y = 9M;

            var imgWidth = Convert.ToDecimal(img.Width);
            var imgHeight = Convert.ToDecimal(img.Height);

            //if (imgWidth / imgHeight > (aspectRatio_X / aspectRatio_Y))
            //{
                var extraWidth = imgWidth - (imgHeight * (aspectRatio_X / aspectRatio_Y));
                var cropStartFrom = extraWidth / 2M;
                var bmp = new Bitmap((int)(img.Width - extraWidth), img.Height, PixelFormat.Format32bppArgb);
                var grp = Graphics.FromImage(bmp);
                grp.DrawImage(img, new Rectangle(0, 0, (int)(img.Width - extraWidth), img.Height), new Rectangle((int)cropStartFrom, 0, (int)(imgWidth - extraWidth), img.Height), GraphicsUnit.Pixel);
                return bmp;
         //   }

            //return null;
        }
    }
}