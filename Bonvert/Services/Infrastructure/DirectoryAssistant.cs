﻿using System.IO;

namespace Bonvert.Services.Infrastructure
{
    public static class DirectoryAssistant
    {
        public static void ReplaceDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }

            Directory.CreateDirectory(path);
        }
    }
}
