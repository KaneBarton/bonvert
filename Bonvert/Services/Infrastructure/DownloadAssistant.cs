﻿using System.Net;
using Bonvert.Annotations;
using Bonvert.Services.Infrastructure.Interfaces;

namespace Bonvert.Services.Infrastructure
{
    [UsedImplicitly]
    public class DownloadAssistant : IDownloadAssistant
    {
        public void DownloadFile(string uri, string destinationFilePath)
        {
            if (string.IsNullOrEmpty(uri)) return;
            if (string.IsNullOrEmpty(destinationFilePath)) return;
            if (uri.Contains("N/A")) return;
            if (destinationFilePath.Contains("N/A")) return;

            using (var client = new WebClient())
            {
                client.Proxy = null;
                client.DownloadFile(uri, destinationFilePath);
            }
        }
    }
}