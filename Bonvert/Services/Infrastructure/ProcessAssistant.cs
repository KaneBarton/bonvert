﻿using System;
using System.Diagnostics;
using Bonvert.Annotations;
using Bonvert.Services.Infrastructure.Interfaces;

namespace Bonvert.Services.Infrastructure
{
    [UsedImplicitly]
    public class ProcessAssistant : IProcessAssistant
    {
        public void DisplayCommandData(object sender, DataReceivedEventArgs e)
        {
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(e.Data);
        }
    }
}
