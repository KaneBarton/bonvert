﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.IO;
using Bonvert.Services.Infrastructure.Interfaces;

namespace Bonvert.Services.Infrastructure
{
    public class ImageAspectAssistance : IImageAspectAssistance
    {
        private readonly IFileAssistant _fileAssistant;

        public ImageAspectAssistance(IFileAssistant fileAssistant)
        {
            _fileAssistant = fileAssistant;
        }

        public void ResizeImageToCorrectAspectRatio(string imagePath)
        {
            // wait until the file has been downloaded
            if (_fileAssistant.IsFileLocked(imagePath, 60)) return;

            var downloadedImage = Image.FromFile(imagePath);
            var correctAspectRatioImage = ImageAssistant.ResizeImg(downloadedImage, 600);

            downloadedImage.Dispose();
            File.Delete(imagePath);

            // wait for the image to be resized
            if (!_fileAssistant.IsFileLocked(imagePath, 60))
            {
                correctAspectRatioImage.Save(imagePath);
            }

            _fileAssistant.IsFileLocked(imagePath, 60);
        }

        private Image ImageCrop(Image image, string imagePath, int width, int height, AnchorPosition anchor)
        {
            const int sourceX = 0;
            const int sourceY = 0;

            var sourceWidth = image.Width;
            var sourceHeight = image.Height;
            var destX = 0;
            var destY = 0;

            float nPercent;

            var nPercentW = (Convert.ToSingle(width) / Convert.ToSingle(sourceWidth));
            var nPercentH = (Convert.ToSingle(height) / Convert.ToSingle(sourceHeight));

            if (nPercentH < nPercentW)
            {
                nPercent = nPercentW;
                switch (anchor)
                {
                    case AnchorPosition.Top:
                        destY = 0;
                        break;
                    case AnchorPosition.Bottom:
                        destY = Convert.ToInt32(height - (sourceHeight * nPercent));
                        break;
                    default:
                        destY = Convert.ToInt32((height - (sourceHeight * nPercent)) / 2);
                        break;
                }
            }
            else
            {
                nPercent = nPercentH;
                switch (anchor)
                {
                    case AnchorPosition.Left:
                        destX = 0;
                        break;
                    case AnchorPosition.Right:
                        destX = Convert.ToInt32((width - (sourceWidth * nPercent)));
                        break;
                    default:
                        destX = Convert.ToInt32(((width - (sourceWidth * nPercent)) / 2));
                        break;
                }
            }

            var destWidth = Convert.ToInt32((sourceWidth * nPercent));
            var destHeight = Convert.ToInt32((sourceHeight * nPercent));

            var bmPhoto = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var grPhoto = Graphics.FromImage(bmPhoto))
            {
                grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                grPhoto.SmoothingMode = SmoothingMode.HighQuality;
                grPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality;
                grPhoto.CompositingQuality = CompositingQuality.HighQuality;
                grPhoto.DrawImage(image, new Rectangle(destX, destY, destWidth, destHeight), new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight), GraphicsUnit.Pixel);
                grPhoto.Dispose();
            }



            return bmPhoto;
        }

        public enum Dimensions
        {
            Width,
            Height
        }

        public enum AnchorPosition
        {
            Top,
            Center,
            Bottom,
            Left,
            Right
        }
    }
}
