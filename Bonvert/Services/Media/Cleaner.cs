﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Bonvert.Annotations;
using Bonvert.Domain;
using Bonvert.Services.Media.Interfaces;

namespace Bonvert.Services.Media
{
    [UsedImplicitly]
    public sealed class Cleaner : ICleaner
    {
        private readonly IMediaLibrary _mediaLibrary;
        private readonly IConversion _conversion;

        public Cleaner(IMediaLibrary mediaLibrary, IConversion conversion)
        {
            _mediaLibrary = mediaLibrary;
            _conversion = conversion;
        }

        public IConversion RemoveOrphanedConvertedFiles()
        {
            Console.WriteLine();
            Console.WriteLine("Starting Cleaner");
            Console.WriteLine("==================");

            if (!_mediaLibrary.AllPathsMapped)
            {
                Console.WriteLine("Some paths are not available, skipping the orphan file clean up");
                Console.WriteLine("Non mapped paths are " + string.Join(", ", _mediaLibrary.NonMappedPaths));
                
                return _conversion;
            }

            // DBORs
            var libraryFiles = _mediaLibrary.Media.Select(x => x.Name).ToArray();
            var convertedFiles = ScanConversionFolder();

            // differential
            var orphanedFiles = convertedFiles.Keys.Except(libraryFiles).ToList();

            if (!orphanedFiles.Any())
            {
                Console.WriteLine("No orphaned files to remove");                
            }
            
            // deleter
            foreach (var file in orphanedFiles)
            {
                Console.WriteLine("Removing orphaned file " + convertedFiles[file]);
                if (!System.Diagnostics.Debugger.IsAttached)
                {
                    //File.Delete(convertedFiles[file]);                    
                }
            }

            return _conversion;
        }

        private IDictionary<string, string> ScanConversionFolder()
        {
            var files = Directory.GetFiles(_mediaLibrary.OutputPath);

            var result = new Dictionary<string, string>();
            foreach (var file in files)
            {
                var fileName = Path.GetFileNameWithoutExtension(file);
                if (!string.IsNullOrEmpty(fileName))
                {
                    // check for duplicates
                    if (result.ContainsKey(fileName))
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine("Duplicate file " + fileName);
                        Console.ForegroundColor = ConsoleColor.White;
                        continue;
                    }

                    result.Add(fileName, fileName);                    
                }
            }

            return result;
            //return files.ToDictionary(Path.GetFileNameWithoutExtension);
        }
    }
}