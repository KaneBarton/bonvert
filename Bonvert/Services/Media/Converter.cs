﻿using System;
using System.Linq;
using Bonvert.Annotations;
using Bonvert.Domain;
using Bonvert.Services.Infrastructure.Interfaces;
using Bonvert.Services.Media.Interfaces;

namespace Bonvert.Services.Media
{
    [UsedImplicitly]
    public class Converter : IConversion
    {
        private readonly IMediaLibrary _mediaLibrary;
        private readonly IRenamer _renamer;
        private readonly IProcessAssistant _processAssistant;

        public Converter(IMediaLibrary mediaLibrary, IRenamer renamer, IProcessAssistant processAssistant)
        {
            _mediaLibrary = mediaLibrary;
            _renamer = renamer;
            _processAssistant = processAssistant;
        }

        public IRenamer LogMediaFilesToConvert()
        {
            ExecuteMissingFileConversion(false);

            return _renamer;
        }

        public IRenamer ConvertMissingMediaFiles()
        {
            ExecuteMissingFileConversion();

            return _renamer;
        }

        private void ExecuteMissingFileConversion(bool executeHandbrakeConversionProcess = true)
        {
            Console.WriteLine();
            Console.WriteLine("Starting Converter");
            Console.WriteLine("==================");

            ConvertedFiles = 0;

            foreach (var model in _mediaLibrary.Media.Where(x => !x.IsConverted))
            {
                Console.WriteLine("Converting {0}", model.Name);
                if (executeHandbrakeConversionProcess)
                {
                    using (var process = new System.Diagnostics.Process())
                    {
                        process.StartInfo.FileName = @"C:\Program Files\Handbrake\HandbrakeCLI.exe";
                        process.StartInfo.Arguments = model.ConversionCommand;

                        process.StartInfo.UseShellExecute = false;
                        process.StartInfo.RedirectStandardOutput = true;
                        process.StartInfo.RedirectStandardError = true;
                        process.StartInfo.CreateNoWindow = true;
                        
                        if (!System.Diagnostics.Debugger.IsAttached)
                        {
                            process.Start();
                            process.BeginOutputReadLine();
                            process.BeginErrorReadLine();
                            process.WaitForExit();
                        }

                       
                    }  
                }

                ConvertedFiles++;
            }
        }

        public int ConvertedFiles { get; private set; }
    }
}