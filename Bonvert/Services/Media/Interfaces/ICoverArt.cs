﻿namespace Bonvert.Services.Media.Interfaces
{
    public interface ICoverArt
    {
        void ApplyCoverArtForConvertedMedia();
    }
}