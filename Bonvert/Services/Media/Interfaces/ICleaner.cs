﻿namespace Bonvert.Services.Media.Interfaces
{
    public interface ICleaner 
    {
        IConversion RemoveOrphanedConvertedFiles();
    }
}