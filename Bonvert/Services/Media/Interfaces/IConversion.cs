﻿namespace Bonvert.Services.Media.Interfaces
{
    public interface IConversion
    {
        IRenamer LogMediaFilesToConvert();
        IRenamer ConvertMissingMediaFiles();
        int ConvertedFiles { get; }
    }
}