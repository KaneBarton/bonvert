﻿namespace Bonvert.Services.Media.Interfaces
{
    public interface IScanner 
    {
        ICleaner ScanForMedia();
    }
}