﻿namespace Bonvert.Services.Media.Interfaces
{
    public interface IRenamer
    {
        ICoverArt RenameMediaLargerThanFat32();
        ICoverArt LogMediaLargerThanFat32();
    }
}