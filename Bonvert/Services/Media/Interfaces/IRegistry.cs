﻿namespace Bonvert.Services.Media.Interfaces
{
    public interface IRegistry
    {
        IScanner RegisterApplicationSettings();
    }
}