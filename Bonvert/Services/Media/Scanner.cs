﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Bonvert.Annotations;
using Bonvert.Domain;
using Bonvert.Services.Media.Interfaces;

namespace Bonvert.Services.Media
{
    [UsedImplicitly]
    public class Scanner : IScanner
    {
        private readonly IMediaLibrary _mediaLibrary;
        private readonly ICleaner _cleaner;

        public Scanner(IMediaLibrary mediaLibrary, ICleaner cleaner)
        {
            _mediaLibrary = mediaLibrary;
            _cleaner = cleaner;
        }

        public virtual ICleaner ScanForMedia()
        {
            Console.WriteLine();
            Console.WriteLine("Starting Scanner");
            Console.WriteLine("==================");

            foreach (var path in _mediaLibrary.LibraryPath)
            {
                if (!Directory.Exists(path))
                {
                    Console.WriteLine("Skipping path " + path);
                }
                else
                {
                    foreach (var file in ScanFiles(path).Where(IsValidExtension))
                    {
                        _mediaLibrary.AddMedia(file);
                    }
                }
            }

            Console.WriteLine("Found " + _mediaLibrary.Media.Count() + " media files");

            return _cleaner;
        }

        internal virtual IEnumerable<string> ScanFiles(string folder)
        {
            return Directory.GetFiles(folder);
        }

        internal virtual bool IsValidExtension(string file)
        {
            switch (Path.GetExtension(file.ToLowerInvariant()))
            {
                case ".m4v":
                case ".mp4":
                case ".avi":
                case ".mkv":
                case ".m2ts":
                    return true;
                default:
                    return false;
            }
        }
    }
}