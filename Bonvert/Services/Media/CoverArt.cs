﻿using System;
using System.Linq;
using Bonvert.Annotations;
using Bonvert.Domain;
using Bonvert.Services.CovertArt.Interfaces;
using Bonvert.Services.Infrastructure.Interfaces;
using Bonvert.Services.Media.Interfaces;

namespace Bonvert.Services.Media
{
    [UsedImplicitly]
    public class CoverArt : ICoverArt
    {
        private readonly IMediaLibrary _mediaLibrary;
        private readonly ISearchProvider _searchProvider;
        private readonly IProcessAssistant _processAssistant;
        private readonly IImageAspectAssistance _imageAspectAssistance;

        public CoverArt(IMediaLibrary mediaLibrary, ISearchProvider searchProvider, IProcessAssistant processAssistant, IImageAspectAssistance imageAspectAssistance)
        {
            _mediaLibrary = mediaLibrary;
            _searchProvider = searchProvider;
            _processAssistant = processAssistant;
            _imageAspectAssistance = imageAspectAssistance;
        }

        public void ApplyCoverArtForConvertedMedia()
        {
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Starting Cover Art");
            Console.WriteLine("==================");

            // get missing cover art
            foreach (var model in _mediaLibrary.Media.Where(x => x.IsConverted && x.NeedsCoverArt && !x.HasDownloadedCoverArt))
            {
                Console.WriteLine("Downloading covert art for {0}", model.Name);
                try
                {
                    _searchProvider.SearchForAndDownloadCovertArt(model);
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                }
            }

            // apply the cover art
            foreach (var model in _mediaLibrary.Media.Where(x => x.IsConverted && x.NeedsCoverArt && x.HasDownloadedCoverArt && !x.IsLargerThanFat32))
            {
                Console.WriteLine("Fixing 16:9 aspect ratio");
                _imageAspectAssistance.ResizeImageToCorrectAspectRatio(model.CoverArtFileName);

                Console.WriteLine("Applying covert art for {0}", model.Name);
                using (var process = new System.Diagnostics.Process())
                {
                    process.StartInfo.FileName = @"Tools\AtomicParsley";
                    process.StartInfo.Arguments = model.CovertArtCommand;

                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.RedirectStandardError = true;
                    process.StartInfo.CreateNoWindow = true;

                    if (_processAssistant != null)
                    {
                        process.ErrorDataReceived += _processAssistant.DisplayCommandData;
                        process.OutputDataReceived += _processAssistant.DisplayCommandData;
                        process.EnableRaisingEvents = true;
                    }

                    if (!System.Diagnostics.Debugger.IsAttached)
                    {
                        process.Start();
                        process.BeginOutputReadLine();
                        process.BeginErrorReadLine();
                        process.WaitForExit();
                    }
                }
            }
        }
    }
}