﻿using System;
using System.Linq;
using Bonvert.Annotations;
using Bonvert.Domain;
using Bonvert.Services.Media.Interfaces;

namespace Bonvert.Services.Media
{
    [UsedImplicitly]
    public class Renamer : IRenamer
    {
        private readonly IMediaLibrary _mediaLibrary;
        private readonly ICoverArt _coverArt;

        public Renamer(IMediaLibrary mediaLibrary, ICoverArt coverArt)
        {
            _mediaLibrary = mediaLibrary;
            _coverArt = coverArt;
        }

        public ICoverArt RenameMediaLargerThanFat32()
        {
            Console.WriteLine();
            Console.WriteLine("Starting Renamer");
            Console.WriteLine("==================");

            foreach (var model in _mediaLibrary.Media.Where(x => x.IsConverted && x.NeedsCoverArt && x.IsM4V && x.IsLargerThanFat32))
            {
                Console.WriteLine();
                Console.WriteLine("Renaming converted file to MP4 due to being larger than 2GB " + model.Name);

                model.RenameToMp4();
            }

            return _coverArt;
        }

        public ICoverArt LogMediaLargerThanFat32()
        {
            Console.WriteLine();
            Console.WriteLine("Starting Renamer");
            Console.WriteLine("==================");

            foreach (var model in _mediaLibrary.Media.Where(x => x.IsConverted && x.NeedsCoverArt && x.IsM4V && x.IsLargerThanFat32))
            {
                Console.WriteLine();
                Console.WriteLine("Renaming converted file to MP4 due to being larger than 2GB " + model.Name);
            }

            return _coverArt;
        }
    }
}