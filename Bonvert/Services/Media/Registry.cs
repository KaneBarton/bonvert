﻿using System;
using Bonvert.Domain;
using Bonvert.Services.Media.Interfaces;

namespace Bonvert.Services.Media
{
    public class Registry : IRegistry
    {
        private readonly IMediaLibrary _mediaLibrary;
        private readonly IScanner _scanner;

        public Registry(IMediaLibrary mediaLibrary, IScanner scanner)
        {
            _mediaLibrary = mediaLibrary;
            _scanner = scanner;
        }

        public IScanner RegisterApplicationSettings()
        {
            Console.WriteLine();
            Console.WriteLine("Starting Registry");
            Console.WriteLine("==================");

            Console.WriteLine("Output Path: " + _mediaLibrary.OutputPath);
            Console.WriteLine("Library Path: " + string.Join(";", _mediaLibrary.LibraryPath));

            return _scanner;
        }
    }
}