﻿namespace Bonvert
{
    public static class StringExtensions
    {
        public static string RemoveTrailingSlash(this string value)
        {
            if (value.EndsWith(@"\"))
            {
                value = value.Substring(0, value.Length - 1);
            }

            return value;
        }
    }
}
