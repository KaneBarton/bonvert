﻿using System.Reflection;
using Autofac;
using Bonvert.Domain;

namespace Bonvert
{
    public static class Application
    {
        static Application()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<MediaLibrary>().As<IMediaLibrary>().SingleInstance();
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).Except<IMediaLibrary>().Except<MediaLibrary>().AsImplementedInterfaces();
            Container = builder.Build();
        }

        internal static IContainer Container { get; private set; }
    }
}